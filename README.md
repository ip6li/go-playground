# test-go

# Certificate

Create a selfsigned certificate

    openssl genrsa -out server.key 2048
    openssl ecparam -genkey -name secp384r1 -out server.key
    openssl req -new -x509 -sha256 -key server.key -out server.crt -days 3650 -subj '/CN=localhost'


# Test

Let's try post and other requests

    curl --data '{}' --insecure --verbose 'https://localhost:8443/rest'
    curl --request DELETE --insecure --verbose 'https://localhost:8443/rest'
    curl --request GET --insecure --verbose 'https://localhost:8443/rest'
    
