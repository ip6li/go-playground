package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

type Foo struct {
	Foo string
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	err, _ := fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[0:])
	if err != 0 {
		fmt.Print("fucked up")
	}
}

func errMsgHandler(w http.ResponseWriter) {
	err, _ := fmt.Fprintf(w, "Sorry, unsupported method")
	if err != 0 {
		return
	}
}

func restHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	switch strings.ToLower(r.Method) {
	case "get":
		err, _ := fmt.Fprintf(w, "this is rest handler %s! (mode: %s)", r.URL.Path[0:], r.Method)
		if err != 0 {
			return
		}
		break
	case "post":
		_, err1 := fmt.Fprintf(w, "this is rest handler %s! (mode: %s)", r.URL.Path[0:], r.Method)
		if err1 != nil {
			return
		}
		body, err2 := ioutil.ReadAll(r.Body)
		if err2 != nil {
			return
		}
		_, err3 := w.Write(body)
		if err3 != nil {
			return
		}

		var inventory Foo
		if jsonErr := json.Unmarshal(body, &inventory); jsonErr != nil {
			log.Fatal(jsonErr)
		}
		fmt.Printf("Foo: %s\n", inventory.Foo)
		break
	case "delete":
		_, err := fmt.Fprintf(w, "this is rest handler %s! (mode: %s)", r.URL.Path[0:], r.Method)
		if err != nil {
			return
		}
		break
	default:
		errMsgHandler(w)
		break
	}
}

func getCurrentDirectory() {
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(path)
}

func main() {
	getCurrentDirectory()
	tls := true
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/rest", restHandler)
	var err error = nil
	if tls {
		err := http.ListenAndServeTLS(":8443", "cert/server.crt", "cert/server.key", nil)
		if err != nil {
			log.Fatal("ListenAndServe: ", err)
		}
	} else {
		err = http.ListenAndServe(":8080", nil)
	}

	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
